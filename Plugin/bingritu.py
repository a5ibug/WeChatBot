# coding=utf8
import os
import urllib.request
import datetime
from itchat.content import *
import itchat

if __name__ != '__main__':
    print("[+]Bing日图插件加载")


def get_msg(msg):
    if msg['Type'] == 'Text':
        if msg['Text'] == '日图':
            itchat.send_image(get_img(), msg['FromUserName'])


def get_img():
    if not os.path.exists('./' + datetime.datetime.now().strftime('%Y-%m-%d') + '.jpg'):
        img_src = 'https://api.dujin.org/bing/1920.php'
        urllib.request.urlretrieve(img_src, './temp/bing/%s.jpg' % datetime.datetime.now().strftime('%Y-%m-%d'))
        return './temp/bing/%s.jpg' % datetime.datetime.now().strftime('%Y-%m-%d')
        print('必应日图下载完成\n')
