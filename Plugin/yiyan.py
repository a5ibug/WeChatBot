# coding=utf8
import requests
from itchat.content import *
import itchat

if __name__ != '__main__':
    print('[+]一言插件加载')


def get_msg(msg):
    if msg['Type'] == 'Text':
        if msg['Text'] == '一言':
            print("[*]获取一言")
            itchat.send_msg(yiyan(), msg['FromUserName'])


def yiyan():
    try:
        r = requests.get('https://v1.hitokoto.cn/?encode=json')
        yiyanjson = r.json()
        msg = yiyanjson['hitokoto'] + "  ----" + yiyanjson['from']
    except BaseException:
        msg = "接口错误,请重试!"
    return msg
    # 一言api结束
