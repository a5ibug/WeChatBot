# coding=utf-8
from glob import glob
from keyword import iskeyword
from os.path import dirname, join, split, splitext
from itchat.content import *
import itchat

basedir = dirname(__file__)
modulelist = []
print("[*]插件加载")
for name in glob(join(basedir, '*.py')):
    module = splitext(split(name)[-1])[0]
    if not module.startswith('_') and \
            module.isidentifier() and \
            not iskeyword(module):
        __import__(__name__ + '.' + module)
        modulelist.append(module)
        # print(module) 模块名称 后续添加判断是否启用
@itchat.msg_register([TEXT, PICTURE, MAP, CARD, SHARING, RECORDING, ATTACHMENT, VIDEO])
@itchat.msg_register([TEXT, PICTURE, MAP, CARD, SHARING, RECORDING, ATTACHMENT, VIDEO], isGroupChat=True)
def get_msg(text):
    for i in modulelist:
        eval(i).get_msg(text)
