#coding=utf8
import requests
import re
import datetime
from itchat.content import *
import itchat

if  __name__ != '__main__':
    print('[+]天气插件加载')

def get_msg(msg):
    if msg['Type'] == 'Text':
        if "天气" in msg['Text']:
            print("[*]获取天气")
            itchat.send_msg(get_weather(regular(msg['Text'],"天气")), msg['FromUserName'])

def get_weather(diqu):
    try:
        r = requests.get('http://wthrcdn.etouch.cn/weather_mini?city='+diqu)
        tqjson = r.json()
        if tqjson['status']==1000 :
            rgx = re.compile("\<\!\[CDATA\[(.*?)\]\]\>")#正则
            high = tqjson['data']['forecast'][0]['high']#最高温
            low = tqjson['data']['forecast'][0]['low']#最低温
            type = tqjson['data']['forecast'][0]['type']#天气类型 晴 多云等
            fengxiang = tqjson['data']['forecast'][0]['fengxiang']#风向
            fengli = rgx.search(tqjson['data']['forecast'][0]['fengli']).group(1).replace('<','小于').replace('>','大于')
            tishi = tqjson['data']['ganmao'];#提示感冒等信息
            nowTime=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            msg = "当前时间:"+nowTime+"\n"+diqu+'今日天气:'+ type +'\n最高温度:'+ high + '\n最低温度:' + low + '\n风向:' + fengxiang + "    " + fengli + '\n' + tishi
        else:
            msg="位置错误"
    except BaseException:
        msg = '天气接口错误'
    return msg
def regular(text,text2):
    return (text.replace(" ","")).replace(text2,"")