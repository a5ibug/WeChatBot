# coding=utf8
from itchat.content import *
import threading
import itchat
import json
import requests
import re
import configparser
import time
import qrcode
from urllib import parse
from bs4 import BeautifulSoup

if __name__ != '__main__':
    print("[+]BOSS直聘插件加载")
    config = configparser.ConfigParser()
    config.readfp(open('config.ini'))
    tempcookie = config.get("BASE", "Cookies")
    cookie = dict(wt=tempcookie, t=tempcookie)
    g_headers = {
        "Host": "www.zhipin.com",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"
                      " AppleWebKit/537.36 (KHTML, like Gecko)"
                      " Chrome/71.0.3578.98 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,"
                  "application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7",
    }

    p_headers = {
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"
                      " AppleWebKit/537.36 (KHTML, like Gecko)"
                      " Chrome/71.0.3578.98 Safari/537.36",
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    }

def get_msg(msg):
    if msg['Type'] == 'Text':
        if msg['Text'] == 'boss直聘登录状态':
            print("[*]boss直聘登陆检测")
            itchat.send_msg(if_login(), msg['FromUserName'])
        elif msg['Text'] == '火爆职位已用':
            get_fire(msg)
        elif msg['Text'] == '获取职位列表':
            get_wrok(msg)
        elif msg['Text'] == 'boss直聘登录':
            itchat.send_msg("生成登录二维码", msg['FromUserName'])
            boss_login(msg)
        elif "自动打招呼" in msg['Text']:
            print("[-]自动打招呼")
            t1 = threading.Thread(target=auto_greet, args=(msg, regular(msg['Text'], "自动打招呼")))
            itchat.send_msg("创建线程,开始自动打招呼", msg['FromUserName'])
            t1.start()
            t1.join()

def get_fire(msg):
    url = "https://www.zhipin.com/privilege/paid/detail.json?business=2"
    # try:
    res = requests.get(url, cookies=cookie)
    for i in range(len(res.json()['data']['jobPrivilegeList'])):
        itchat.send_msg(
            "职位：{} 最大次数：{} 已用次数：{} 剩余次数:{}".format(res.json()['data']['jobPrivilegeList'][i]['positionName'],
                                                   res.json()['data']['jobPrivilegeList'][i][
                                                       "jobDayUsedPrivilegeList"][1]["privilegeCount"],
                                                   res.json()['data']['jobPrivilegeList'][i][
                                                       "jobDayUsedPrivilegeList"][1]["privilegeUsedCount"],
                                                   res.json()['data']['jobPrivilegeList'][i][
                                                       "jobDayUsedPrivilegeList"][1]["privilegeCount"] -
                                                   res.json()['data']['jobPrivilegeList'][i][
                                                       "jobDayUsedPrivilegeList"][1]["privilegeUsedCount"]),
            msg['FromUserName'])
        if (i == 0):  # 只有一个职位
            return True


def if_login():
    if tempcookie != '':
        if (login_state()):
            print("[+]登陆成功")
            return '登陆成功Cookies可用\n{}'.format(welcomeinfo())
        else:
            print("[-]登陆失败")
            return '登录失败,cookies失效'
    else:
        return 'cookies不存在'


def login_state():
    url = "https://www.zhipin.com/geek/attresume/checkbox.json"
    try:
        res = requests.get(url, cookies=cookie)
        if res.json()['rescode'] == 1:
            return True
        else:
            return False
    except BaseException:
        print("[-]登陆失败,api网络错误")
        return '检测异常,可能为网络错误或api错误'


def welcomeinfo():
    url = "https://www.zhipin.com/boss/user/info.html"
    try:
        res = requests.get(url, cookies=cookie)
        name = re.findall(re.compile('class="c">(.*?)<'), res.text)
        return ("欢迎您:{}".format(name[0]))
    except(BaseException):
        print("[-]检测异常,可能为网络错误或api错误")
        return '姓名获取异常'


def boss_login(msg):
    print("[*]获取qrId")
    itchat.send_msg('获取qrId', msg['FromUserName'])
    try:
        qrId = json.loads(requests.get("https://login.zhipin.com/wapi/zppassport/captcha/randkey").text)["zpData"][
            "qrId"]
        print("[+]qrId获取成功,qrId：{}".format(qrId))
        itchat.send_msg("qrId获取成功,qrId：{}".format(qrId), msg['FromUserName'])
    except(BaseException):
        print("[-]qrId获取异常,可能为网络错误或api错误")
        itchat.send_msg("qrId获取异常,可能为网络错误或api错误", msg['FromUserName'])
    time.sleep(1)
    print("[*]根据qrId生成二维码")
    itchat.send_msg("根据qrId生成二维码", msg['FromUserName'])
    img = qrcode.make(qrId)
    img.save(r"./temp/qrcode.png")
    itchat.send_image(r"./temp/qrcode.png", msg['FromUserName'])
    headers = {
        "Host": "www.zhipin.com",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"
                      " AppleWebKit/537.36 (KHTML, like Gecko)"
                      " Chrome/71.0.3578.98 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,"
                  "application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Referer": "https://www.zhipin.com/user/login.html",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7",
    }
    for i in range(5):
        if (json.loads(requests.get("https://login.zhipin.com/scan?uuid={}".format(qrId)).text)['scaned']):
            url = "https://www.zhipin.com/qrscan/dispatcher?qrId={}".format(qrId)
            res = requests.get(url, headers=headers)
            if (res.url != "https://www.zhipin.com/"):
                print("[*]登陆成功\n{}".format(welcomeinfo()))
                itchat.send_msg("登陆成功,向配置文件写入cookies\n{}".format(welcomeinfo()), msg['FromUserName'])
                session = requests.session()
                session.get(url)
                cookie = dict(wt=session.cookies.get_dict()["wt"], t=session.cookies.get_dict()["wt"])
                # 此处向配置文件写入cookies
                config.set("BASE", "Cookies", session.cookies.get_dict()["wt"])
                config.write(open('config.ini', "r+"))
                return True
            else:
                exit("[-]程序异常~")
        else:
            print("[*]第{}次登录失败,2秒后检测\n".format(i))
            itchat.send_msg("第{}次登录失败,2秒后检测".format(i), msg['FromUserName'])
        time.sleep(2)
    return False


def get_wrok(msg):
    print("[*]获取职位")
    res = requests.get("https://www.zhipin.com/bossweb/joblist/data.json", cookies=cookie)
    test  = re.findall(re.compile('jobedit/(.*?)</a><em cla'), res.json()['html'])
    for i in test:
        id = i.replace(".html\">", " ").split()[0]
        name = i.replace(".html\">", " ").split()[1]
        itchat.send_msg("职位id:{},职位名称：{}".format(id, name), msg['FromUserName'])

def get_list(msg, jobid):
    print("[*]获取列表牛人")
    url="https://www.zhipin.com/boss/recommend/geeks.json?page=1&status=1&jobid={}".format(jobid)
    res = requests.get(url, cookies=cookie, headers=g_headers)
    if res.json()["rescode"] != 1:
        print("[-]获取列表错误")
        return False
    recommend_list = []
    html = BeautifulSoup(res.json()["htmlList"], "lxml")
    for i in html.select("a"):
        greeting_status = i.findPreviousSibling().find("button").text
        data = {
            "name": i.select_one(".geek-name").text,
            "uid": i.attrs["data-uid"],
            "eid": i.attrs["data-eid"],
            "expect": i.attrs["data-expect"],
            "jid": i.attrs["data-jid"],
            "lid": i.attrs["data-lid"],
            "canGreeting": True if greeting_status == "打招呼" else False,
            "href": i.attrs["href"]
        }
        recommend_list.append(data)
    print("[+]已获取牛人{}位".format(len(recommend_list)))
    itchat.send_msg("已获取牛人{}位".format(len(recommend_list)), msg['FromUserName'])
    return recommend_list

def auto_greet(msg,jobid):
    list = get_list(msg, jobid)
    url = "https://www.zhipin.com/privilege/paid/detail.json?business=2"
    res = requests.get(url, cookies=cookie)
    for applicant in list:
        print("[*]开始对{}打招呼".format(applicant['name']))
        itchat.send_msg("开始对{}打招呼".format(applicant['name']), msg['FromUserName'])
        url = "https://www.zhipin.com/chat/batchAddRelation.json"
        params = {
            "gids": applicant["uid"],
            "jids": applicant["jid"],
            "expectIds": applicant["expect"],
            "lids": applicant["lid"]
        }
        res = requests.post(url, data=parse.urlencode(params), headers=p_headers, cookies=cookie)
        if res.status_code != 200:
            print("[-]打招呼失败")
            itchat.send_msg("打招呼失败", msg['FromUserName'])
        else:
            itchat.send_msg("打招呼成功,7秒后向下一个牛人打招呼", msg['FromUserName'])
            time.sleep(7)
    itchat.send_msg("已完成一组打招呼", msg['FromUserName'])
def regular(text, text2):
    return (text.replace(" ", "")).replace(text2, "")
